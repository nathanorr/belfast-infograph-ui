import React, { Component } from "react";
import ParticlesBg from "particles-bg";
import axios from "axios";
import Clock from "react-live-clock";

export default class Display extends Component {
  constructor() {
    super();
    this.state = {
      message: ""
    };
  }

  componentDidMount() {
    this.getMessage();
  }

  async getMessage() {
    await axios({
      method: "GET",
      url:
        "https://d0ilv7h4ij.execute-api.eu-west-1.amazonaws.com/default/getWelcomeScreenData",
      headers: {
        "Content-Type": "application/json",
        Accept: "*/*"
      }
    })
      .then(response => {
        this.setState({ message: response.data });
      })
      .catch(function(error) {
        console.log("Error getting data from db");
      });
  }

  state = {
    date: new Date()
  };

  onChange = date => this.setState({ date });

  render() {
    var message1, message2, defaultMessage;

    if (this.state.message.visitorName === undefined) {
      defaultMessage = "Welcome to DD Belfast!";
    } else {
      message1 = this.state.message.visitorName;
      message2 = this.state.message.greeterName;
    }

    return (
      <div>
        <ParticlesBg type='square' bg={true} num={4} />
        <img
          src='ddLogo.png'
          className='img'
          alt='ddlogo'
          href='/Update'
          onClick={() => {
            document.location.href = "/Update";
          }}
        />
        <div className='default-message'>{defaultMessage}</div>
        <div className='upper-message'>{message1}</div>
        <div className='lower-message'>{message2}</div>
        <div>
          <Clock
            format={"dddd MMMM Do"}
            timezone={"GB"}
            className='time time-date'
          />
          <br />
          <Clock
            format={"h:mm"}
            ticking={true}
            timezone={"GB"}
            className='time time-clock'
          />
        </div>
      </div>
    );
  }
}
