import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import UpdateDisplay from "./components/UpdateDisplay";
import Display from "./components/Display";
import DeleteMessages from "./components/DeleteMessages";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

const routing = (
  <Router>
    <Switch>
      <Route path='/display' component={Display} />
      <Route path='/update' component={UpdateDisplay} />
      <Route path='/delete' component={DeleteMessages} />
      <Route path='/' component={Display} />
    </Switch>
  </Router>
);
render(routing, document.getElementById("root"));
