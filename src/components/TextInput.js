import React from "react";

const TextInput = props => {
  return (
    <div>
      <label className='main' id='title1'>
        {props.labelName}
      </label>
      <input
        className='form-control'
        type='text'
        name={props.inputName}
        onChange={props.changeHandler}
        value={props.msg}
      />
    </div>
  );
};

export default TextInput;
