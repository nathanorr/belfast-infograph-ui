# Belfast Infograph Welcome Page UI

## Information
This is the front-end UI component of the welcome component of the Belfast Infograph project.

https://devservices.jira.com/wiki/spaces/DBIG/overview

To start the project locally, run the command `npm start` and it should run on port 3000. You will need to run `npm install` in the root folder when the project is first pulled down from the repository to get the node_modules folder.