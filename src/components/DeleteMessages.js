import React, { Component } from "react";
import axios from "axios";
import BootstrapTable from "react-bootstrap-table-next";
import { Modal, Button, Spinner } from "react-bootstrap";

export default class DeleteMessages extends Component {
  constructor() {
    super();
    this.state = {
      tableData: "",
      show: false,
      isLoading: false,
      buttonText: "Submit",
      selectedRow: ""
    };

    this.showModal = () => {
      this.setState({
        show: !this.state.show
      });
    };

    this.onClose = () => {
      this.setState({
        show: false
      });
    };

    this.deleteMessage = this.deleteMessage.bind(this);
  }

  componentDidMount() {
    this.getMessage();
  }

  async getMessage() {
    await axios({
      method: "GET",
      url:
        "https://d0ilv7h4ij.execute-api.eu-west-1.amazonaws.com/default/getAllWelcomeData",
      headers: {
        "Content-Type": "application/json",
        Accept: "*/*"
      }
    })
      .then(response => {
        this.setState({ tableData: response.data });
      })
      .catch(function(error) {
        console.log("Error getting data from db");
      });
  }

  async deleteMessage(event) {
    this.setState({ isLoading: true });
    event.preventDefault();
    await axios({
      method: "DELETE",
      url:
        "https://d0ilv7h4ij.execute-api.eu-west-1.amazonaws.com/default/getWelcomeScreenData",
      headers: {
        "Content-Type": "application/json",
        Accept: "*/*"
      },
      data: {
        visitorDataID: this.selectedRow
      }
    })
      .then()
      .catch(function(error) {
        console.log("Error getting data from db");
      });
    this.showModal();
    this.setState({ isLoading: false });
    this.getMessage();
  }

  render() {
    var data;
    let submitButton;
    if (!this.state.isLoading) {
      submitButton = (
        <Button type='submit' variant='danger' onClick={this.deleteMessage}>
          <span>{this.state.buttonText}</span>
        </Button>
      );
    } else {
      submitButton = (
        <Button variant='danger' disabled>
          <Spinner
            as='span'
            animation='border'
            size='sm'
            role='status'
            aria-hidden='true'
          />
          <span> {this.state.buttonText}</span>
        </Button>
      );
    }

    const rowEvents = {
      onClick: (e, row, rowIndex) => {
        console.log(row.visitorDataID);
        this.selectedRow = row.visitorDataID;
        this.showModal();
        console.log(this.selectedRow);
      }
    };

    if (this.state.tableData.tableValues === undefined) {
      return (
        <div>
          <div className='header'>
            <h1>Delete Messages</h1>
          </div>
          <div className='container-fluid'>
            <div className='form-message'>
              <h2>Review and remove messages below</h2>
            </div>
            <div>
              <Spinner animation='border' size='xl' role='status'>
                <span className='sr-only'>Loading...</span>
              </Spinner>
            </div>
          </div>
        </div>
      );
    } else {
      data = JSON.parse(this.state.tableData.tableValues);
      const columns = [
        {
          dataField: "visitorDataID",
          text: "Date"
        },
        { dataField: "visitorName", text: "Upper Message" },
        { dataField: "greeterName", text: "Lower Message" },
        { dataField: "contactNumber", text: "Contact" }
      ];
      return (
        <div>
          <div className='header'>
            <h1>Delete Messages</h1>
          </div>
          <div className='container-fluid'>
            <div className='form-message'>
              <h2>Review and remove messages below</h2>
            </div>
            <div>
              <BootstrapTable
                keyField='visitorDataID'
                data={data}
                columns={columns}
                rowEvents={rowEvents}
                hover
              />
            </div>
          </div>
          <Modal show={this.state.show} onHide={this.showModal}>
            <Modal.Body text-align='center'>
              Are you sure you wish to delete the value for {this.selectedRow}?
            </Modal.Body>
            <Modal.Footer>
              <Button variant='primary' onClick={this.showModal}>
                Close
              </Button>
              {submitButton}
            </Modal.Footer>
          </Modal>
        </div>
      );
    }
  }
}
