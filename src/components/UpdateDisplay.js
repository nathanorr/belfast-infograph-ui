import React, { Component } from "react";
import axios from "axios";
import { Modal, Button, Spinner } from "react-bootstrap";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import { formatDate, parseDate } from "react-day-picker/moment";
import TextInput from "./TextInput";

export default class Display extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      message2: "",
      message3: "",
      confirmation: "",
      show: false,
      isLoading: false,
      buttonText: "Submit",
      date: ""
    };

    this.showModal = () => {
      this.setState({
        show: !this.state.show
      });
    };

    this.onClose = () => {
      this.setState({
        show: false
      });
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const inputValue = event.target.value;
    const stateField = event.target.name;
    this.setState({
      [stateField]: inputValue
    });
  }

  async handleSubmit(event) {
    this.setState({ isLoading: true });
    event.preventDefault();
    const { message, message2, message3, date } = this.state;
    let confirmationMessage = "";
    await axios({
      method: "PUT",
      url:
        "https://d0ilv7h4ij.execute-api.eu-west-1.amazonaws.com/default/getWelcomeScreenData",
      headers: {
        "Content-Type": "application/json",
        Accept: "*/*"
      },
      data: {
        visitorName: `${message}`,
        greeterName: `${message2}`,
        contactNumber: `${message3}`,
        meetingTime: date
      }
    })
      .then((confirmationMessage = "Table successfully updated."))
      .catch(function(error) {
        confirmationMessage = "Error updating table.";
      });
    this.state.confirmation = confirmationMessage;
    this.setState({ isLoading: false });
    this.showModal();
  }

  render() {
    let submitButton;
    if (!this.state.isLoading) {
      submitButton = (
        <Button type='submit' variant='primary'>
          <span>{this.state.buttonText}</span>
        </Button>
      );
    } else {
      submitButton = (
        <Button variant='primary' disabled>
          <Spinner
            as='span'
            animation='border'
            size='sm'
            role='status'
            aria-hidden='true'
          />
          <span> {this.state.buttonText}</span>
        </Button>
      );
    }

    return (
      <div>
        <div className='header'>
          <h1>Update Form</h1>
        </div>
        <div className='container-fluid'>
          <div className='form-message'>
            <h2>Please enter the messages for visitors and greeters below</h2>
          </div>
          <form onSubmit={this.handleSubmit}>
            <TextInput
              labelName='Visitor Message:'
              changeHandler={this.handleChange}
              msg={this.state.message}
              inputName='message'
            />

            <TextInput
              labelName='Greeter Message:'
              changeHandler={this.handleChange}
              msg={this.state.message2}
              inputName='message2'
            />
            <TextInput
              labelName='Greeter Contact:'
              changeHandler={this.handleChange}
              msg={this.state.message3}
              inputName='message3'
            />
            <div>
              <label className='main' id='title2'>
                Date:
              </label>
              <br />
              <DayPickerInput
                format='DD/MM/YYYY'
                formatDate={formatDate}
                parseDate={parseDate}
                onDayChange={day =>
                  this.setState({
                    date:
                      ("0" + day.getDate()).slice(-2) +
                      "/" +
                      ("0" + (day.getMonth() + 1)).slice(-2) +
                      "/" +
                      day.getFullYear()
                  })
                }
              />
            </div>
            <br />
            {submitButton}
          </form>
        </div>
        <Modal show={this.state.show} onHide={this.showModal}>
          <Modal.Body>{this.state.confirmation}</Modal.Body>
          <Modal.Footer>
            <Button variant='secondary' onClick={this.showModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
